import 'package:flutter/material.dart';

class addArticle extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
        appBar: AppBar(
          centerTitle: true,
          title: Text(
            'RuangSinggah',
            style: TextStyle(fontWeight: FontWeight.bold, color: Colors.white),
          ),
          backgroundColor: Color(0xff304ffe),
        ),
        body: Container(
          margin: EdgeInsets.all(25),
          child: ListView(
            children: [
              TextFormField(
                decoration: new InputDecoration(
                  hintText: "Masukkan judul tulisanmu disini!",
                  labelText: "Title",
                  icon: Icon(Icons.title),
                  border: OutlineInputBorder(
                      borderRadius: new BorderRadius.circular(5.0)),
                ),
                validator: (value) {
                  if (value == null) {
                    return 'field ini tidak boleh kosong';
                  }
                  return null;
                },
              ),
              Padding(padding: EdgeInsets.only(bottom: 25)),
              TextFormField(
                decoration: new InputDecoration(
                  hintText: "Masukkan namamu disini!",
                  labelText: "Author",
                  icon: Icon(Icons.people),
                  border: OutlineInputBorder(
                      borderRadius: new BorderRadius.circular(5.0)),
                ),
                validator: (value) {
                  if (value == null) {
                    return 'field ini tidak boleh kosong';
                  }
                  return null;
                },
              ),
              Padding(padding: EdgeInsets.only(bottom: 25)),
              TextFormField(
                decoration: new InputDecoration(
                  hintText: "Masukkan deskripsi artikelmu!",
                  labelText: "Deskripsi",
                  icon: Icon(Icons.description),
                  border: OutlineInputBorder(
                      borderRadius: new BorderRadius.circular(5.0)),
                ),
                validator: (value) {
                  if (value == null) {
                    return 'field ini tidak boleh kosong';
                  }
                  return null;
                },
              ),
              Padding(padding: EdgeInsets.only(bottom: 25)),
              TextFormField(
                decoration: new InputDecoration(
                  hintText: "Masukkan artikelmu!",
                  labelText: "Artikel",
                  icon: Icon(Icons.book),
                  border: OutlineInputBorder(
                      borderRadius: new BorderRadius.circular(5.0)),
                ),
                validator: (value) {
                  if (value == null) {
                    return 'field ini tidak boleh kosong';
                  }
                  return null;
                },
              ),
              Padding(padding: EdgeInsets.only(bottom: 25)),
              ElevatedButton(
                  onPressed: () {
                    Navigator.pop(context);
                  },
                  child: Text("Submit"))
            ],
          ),
        ));
  }
}
