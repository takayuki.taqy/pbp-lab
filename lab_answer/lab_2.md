1. Apakah perbedaan antara JSON dan XML?
    | JSON                                    | XML                             |
    | -----------                             | -----------                     |
    | Tidak menggunakan end tag               | Menggunakan start dan end tag   |
    | Dapat menggunakan array                 | Tidak dapat menggunakan array   |
    | Digunakan untuk merepresentasikan objek | Digunakan untuk menampilan data |
    | Lebih mudah untuk dibaca                | Sulit untuk dibaca              |
2. Apakah perbedaan antara HTML dan XML?
    | HTML                                          | XML                                       |
    | -----------                                   | -----------                               |
    | berfokus pada transfer data                   | berfokus pada penyajian data              |
    | Case Sensitive                                | Case Inensitive                           |
    | menyediakan dukungan namespaces               | Tidak menyediakan dukungan namespaces     |
    | Memiliki tag yang sudah ditentukan sebelumnya | Tag dapat dibuat sesuai dengan kebutuhan  |

Referensi: 
1. https://www.w3schools.com/js/js_json_xml.asp
2. https://blogs.masterweb.com/perbedaan-xml-dan-html/
