import 'package:flutter/material.dart';
import 'package:carousel_slider/carousel_slider.dart';
import 'nav-drawer.dart';

void main() {
  runApp(MyApp());
}

class MyApp extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      home: Scaffold(
        appBar: AppBar(
          centerTitle: true,
          title: Text(
            'RuangSinggah',
            style: TextStyle(fontWeight: FontWeight.bold, color: Colors.white),
          ),
          backgroundColor: Color(0xff304ffe),
        ),
        drawer: DrawerWidget(),
        body: Container(
          margin: EdgeInsets.all(25),
          child: ListView(
            children: [
              CarouselSlider(
                items: [
                  //1st Image of Slider
                  Container(
                    margin: EdgeInsets.all(6.0),
                    decoration: BoxDecoration(
                      borderRadius: BorderRadius.circular(8.0),
                      image: DecorationImage(
                        image: NetworkImage("../assets/chat.png"),
                        fit: BoxFit.cover,
                      ),
                    ),
                  ),

                  //2nd Image of Slider
                  Container(
                    margin: EdgeInsets.all(6.0),
                    decoration: BoxDecoration(
                      borderRadius: BorderRadius.circular(8.0),
                      image: DecorationImage(
                        image: NetworkImage("../assets/money.png"),
                        fit: BoxFit.cover,
                      ),
                    ),
                  ),

                  //3rd Image of Slider
                  Container(
                    margin: EdgeInsets.all(6.0),
                    decoration: BoxDecoration(
                      borderRadius: BorderRadius.circular(8.0),
                      image: DecorationImage(
                        image: NetworkImage("../assets/test.png"),
                        fit: BoxFit.cover,
                      ),
                    ),
                  ),
                ],

                //Slider Container properties
                options: CarouselOptions(
                  height: 180.0,
                  enlargeCenterPage: true,
                  autoPlay: true,
                  aspectRatio: 16 / 9,
                  autoPlayCurve: Curves.fastOutSlowIn,
                  enableInfiniteScroll: true,
                  autoPlayAnimationDuration: Duration(milliseconds: 800),
                  viewportFraction: 0.8,
                ),
              ),
              Text(
                "Artikel terbaru untukmu!",
                style: TextStyle(fontWeight: FontWeight.bold),
              ),
              InkWell(
                onTap: () {},
                child: Container(
                  margin: EdgeInsets.only(top: 10),
                  child: ListTile(
                    leading: Image(
                      image: NetworkImage("assets/illus.png"),
                      width: 50,
                    ),
                    title: Text("Lorem Ipsum"),
                    subtitle: Text("Lorem Ipsum Dolor Amit"),
                    tileColor: Colors.white,
                  ),
                  decoration: BoxDecoration(
                      color: Colors.white,
                      borderRadius: BorderRadius.only(
                          topLeft: Radius.circular(10),
                          topRight: Radius.circular(10),
                          bottomLeft: Radius.circular(10),
                          bottomRight: Radius.circular(10)),
                      border: Border.all(color: Colors.black.withOpacity(0.3))),
                ),
              ),
              InkWell(
                onTap: () {},
                child: Container(
                  margin: EdgeInsets.only(top: 10),
                  child: ListTile(
                    leading: Image(
                      image: NetworkImage("assets/illus.png"),
                      width: 50,
                    ),
                    title: Text("Lorem Ipsum"),
                    subtitle: Text("Lorem Ipsum Dolor Amit"),
                    tileColor: Colors.white,
                  ),
                  decoration: BoxDecoration(
                      color: Colors.white,
                      borderRadius: BorderRadius.only(
                          topLeft: Radius.circular(10),
                          topRight: Radius.circular(10),
                          bottomLeft: Radius.circular(10),
                          bottomRight: Radius.circular(10)),
                      border: Border.all(color: Colors.black.withOpacity(0.3))),
                ),
              ),
              InkWell(
                onTap: () {},
                child: Container(
                  margin: EdgeInsets.only(top: 10),
                  child: ListTile(
                    leading: Image(
                      image: NetworkImage("assets/illus.png"),
                      width: 50,
                    ),
                    title: Text("Lorem Ipsum"),
                    subtitle: Text("Lorem Ipsum Dolor Amit"),
                    tileColor: Colors.white,
                  ),
                  decoration: BoxDecoration(
                      color: Colors.white,
                      borderRadius: BorderRadius.only(
                          topLeft: Radius.circular(10),
                          topRight: Radius.circular(10),
                          bottomLeft: Radius.circular(10),
                          bottomRight: Radius.circular(10)),
                      border: Border.all(color: Colors.black.withOpacity(0.3))),
                ),
              ),
              InkWell(
                onTap: () {},
                child: Container(
                  margin: EdgeInsets.only(top: 10),
                  child: ListTile(
                    leading: Image(
                      image: NetworkImage("assets/illus.png"),
                      width: 50,
                    ),
                    title: Text("Lorem Ipsum"),
                    subtitle: Text("Lorem Ipsum Dolor Amit"),
                    tileColor: Colors.white,
                  ),
                  decoration: BoxDecoration(
                      color: Colors.white,
                      borderRadius: BorderRadius.only(
                          topLeft: Radius.circular(10),
                          topRight: Radius.circular(10),
                          bottomLeft: Radius.circular(10),
                          bottomRight: Radius.circular(10)),
                      border: Border.all(color: Colors.black.withOpacity(0.3))),
                ),
              ),
            ],
          ),
        ),
        bottomNavigationBar: BottomNavigationBar(
          items: const <BottomNavigationBarItem>[
            BottomNavigationBarItem(
              icon: Icon(Icons.home),
              title: Text('Artikel'),
            ),
            BottomNavigationBarItem(
              icon: Icon(Icons.money),
              title: Text('Donasi'),
            ),
            BottomNavigationBarItem(
              icon: Icon(Icons.fact_check),
              title: Text('Quiz'),
            ),
            BottomNavigationBarItem(
              icon: Icon(Icons.chat_bubble),
              title: Text('Thread'),
            ),
            BottomNavigationBarItem(
              icon: Icon(Icons.person),
              title: Text('Consultation'),
            ),
          ],
          currentIndex: 0,
          selectedItemColor: Colors.blueAccent[700],
          unselectedItemColor: Colors.grey,
          showUnselectedLabels: true,
        ),
      ),
    );
  }
}
